# 1.1 (2022-01-06)

* Remove RTF markup from search results

# 1.0 (2022-01-05)

* Add "rtf" content model
* Support displaying "rtf" articles as HTML
